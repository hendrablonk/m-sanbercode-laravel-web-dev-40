<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php
    echo "<h3> Soal No 1</h3>";
    $string1 = "PHP is never old";
    Output:
    echo "string 1 : " . $string1 ."<br>"; 
    echo "Panjang string 1 : " . strlen($string1) . "<br>";
    echo "Jumlah kata string 1 :" .str_word_count($string1) . "<br>";
 
    echo "<h3> Soal No 2</h3>";

    $string2 = "I love PHP";
    echo "string 2 : " .$string2 . "<br>";

    echo "kata  1 string 2 : " . substr($string2,0,1) . "<br>";
    echo "kata  2 string 2 : " . substr($string2,2,4) . "<br>";
    echo "kata  3 string 2 : " . substr($string2,7,3) . "<br>";

    echo "<h3> Soal No 3</h3>";

    $string3 = "PHP is old but sexy!";
    echo "string 3 : " . $string3 . "<br>";
    echo "string 3 ganti string : " . str_replace("sexy!","awesome",$string3)."<br>";

    ?> 
</body>
</html>