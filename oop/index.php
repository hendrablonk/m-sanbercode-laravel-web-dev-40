<?php

    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');   

    $sheep = new Animal("shaun");

    echo "Name : . $sheep->name . <br>"; // "shaun"
    echo "legs : . $sheep->legs . <br>"; // 4
    echo "cold_blooded : . $sheep->cold_blooded . <br><br>"; "no"

    $sungokong  = new Ape("kera sakti");
    echo "Name : $sungokong->yell <br>"; // "Auooo"
    echo "Name : $sungokong->name <br>";  
    echo "legs : $sungokong->legs <br>";  
    echo "cold_blooded : $sungokong->cold_blooded<br>";
    echo $sungokong->shout();

    $kodok = new frog("buduk");

    echo "Name :" . $buduk->name . "<br>"; // "frog"
    echo "legs :" . $buduk->legs . "<br>"; //  4
    echo "cold_blooded :" . $buduk->cold_blooded . "<br>"; // "no"
    $kodok->jump(); //"hop hop"

    ?>
