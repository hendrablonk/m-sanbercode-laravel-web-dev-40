<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
    <?php
    echo "<h3>Soal No 1 Looping I Love PHP</h3>";


    $a=2;
    echo "<h5>Looping 1</h5>";
    do {
       echo $a . " - I Love PHP <br>";
       $a+=2;
    } while ($a <= 20);

    echo "<h5>Looping 2</h5>";
    for ($i=20; $i >= 2 ; $i-=2) {
        echo $i . " - I Love PHP <br>";
    }

    echo "<h3>Soal No 2 Looping Array Modulo </h3>";

    $numbers = [18, 45, 29, 61, 47, 34];

    echo "array number : ";
    print_r($numbers);
    
    foreach ($numbers as  $item) {
        $rest[] = $item %= 5;
    }
    echo    "<br>";
    echo "hasil number sisa bagi 5 : ";
    print_r($rest);


    echo "<h3> Soal No 3 Looping Asociative Array </h3>";

    $multiarray = [
        ["001","Keyboard Logitek","60000","Keyboard yang mantap untuk kantoran","logitek.jpeg"],
        ["002","Keyboard MSI","300000","Keyboard gaming MSI mekanik ","msi.jpeg"],
        ["003","Mouse Genius","50000","Mouse Genius biar lebih pinter","genius.jpeg"],
        ["004"," Mouse Jerry","30000","Mouse yang disukai kucing","jerry.jpeg"],
    ];
        foreach ($multiarray as $value) {
           $tampung =[
                'id' => $value[0],
                'name' => $value[1],
                'price' => $value[2],
                'description' => $value[3],
                'source' => $value[4],
           ];
           print_r($tampung);
           echo "<br>";
        }
        
        echo "<h3>Soal No 4 Asterix </h3>";
        
        echo "Asterix: ";
        echo "<br>";       
        $sun=5;
        for($a=$sun;$a>0;$a--){
        for($b=$sun;$b>=$a;$b--){
          echo "*";
        }
        echo "<br>";
        }

?>

</body>
</html>